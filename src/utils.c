#include <stdlib.h>
#include <ncurses.h>

#include "utils.h"

static int setup = 0;

/**
 * @brief Setup ncurses
 *
 */
void setup_terminal()
{
    initscr();
    raw();
    curs_set(0);
    noecho();
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);

    use_default_colors();
    start_color();
    setup = 1;
}

/**
 * @brief garbage collection used by ncurses
 *
 */
void close_terminal()
{
    endwin();
}

__attribute__((noreturn)) void error(char *output)
{
    if (setup)
    {
        close_terminal();
    }
    fprintf(stderr, "%s\n", output);
    exit(EXIT_FAILURE);
}