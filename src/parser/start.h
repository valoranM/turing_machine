#ifndef START_H
#define START_H

#include "run.h"

extern int start_found;

extern int is_start(turing_machine *computer);

#endif /* START_H */