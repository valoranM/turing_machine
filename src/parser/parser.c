#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ncurses.h>

#include "utils.h"

#include "run.h"

#include "parser/parser.h"
#include "parser/blank.h"
#include "parser/input.h"
#include "parser/link.h"
#include "parser/node.h"
#include "parser/start.h"

FILE *file = NULL;
char err[100];
char act_char;
int line, col;

static int escape = 0;

static void pass_comment();
static void keywoord_err();

void read_file(const char *filename, turing_machine *computer)
{
    line = 1;
    col = 0;
    file = fopen(filename, "r");
    if (file == NULL)
    {
        sprintf(err, "error: fopen(%s)", filename);
        error(err);
    }
    do
    {
        next_char();
        if (IS_LETTER(act_char))
        {
            if (is_input(computer->mem))
                ;
            else if (is_blank(computer))
                ;
            else if (is_node(computer))
                ;
            else if (is_start(computer))
                ;
            else if (is_link(computer))
                ;
            else
            {
                keywoord_err();
            }
        }
    } while (act_char != EOF);
    refresh();
}

int check_keyword(const char *word)
{
    int count = 0;
    while (1)
    {
        if (act_char == word[count])
        {
            count++;
            if (word[count] == '\0')
            {
                next_char();
                if (IS_LETTER(act_char))
                {
                    fseek(file, -1 - count, SEEK_CUR);
                    col = col - count;
                    return 0;
                }
                break;
            }
        }
        else
        {
            fseek(file, -count - 1, SEEK_CUR);
            col = col - count - 1;
            next_char();

            return 0;
        }
        next_char();
    }
    pass_space();
    return 1;
}

void pass_point(const char *word)
{
    pass_space();
    if (act_char != ':')
    {
        sprintf(err, "error: expected ':' after %s (L: %d, C:%d)",
                word, line, col);
        error(err);
    }
    next_char();
    pass_space();
}

int next_char()
{
    act_char = fgetc(file);
    col++;
    if (act_char == '\n')
    {
        line++;
        col = 0;
    }
    else if (IS_COMMENT(act_char))
    {
        pass_comment();
        line++;
        col = 0;
    }
    else if (IS_ESCAPE(act_char) && !escape)
    {
        escape_function();
        return 1;
    }
    return 0;
}

void pass_space()
{
    while (IS_SPACE(act_char))
    {
        act_char = fgetc(file);
        col++;
    }
}

void escape_function()
{
    escape = 1;
    next_char();
    switch (act_char)
    {
    case 'n':
        act_char = '\n';
        break;
    case 't':
        act_char = '\t';
        break;
    case '\\':
        act_char = '\\';
        break;
    case '\'':
        act_char = '\'';
        break;
    }
    escape = 0;
}

static void keywoord_err()
{
    sprintf(err, "error: unknown keyword (L: %d, C: %d)\n\t", line, col);
    while (IS_LETTER(act_char))
    {
        strncat(err, &act_char, 1);
        next_char();
    }
    error(err);
}

static void pass_comment()
{
    while (fgetc(file) != '\n')
        ;
}