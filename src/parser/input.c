#include "utils.h"
#include "parser/parser.h"

#define INPUT_VAR "input"

int input_found = 0;

static void extract_input(memory_t *memory);

int is_input(memory_t *memory)
{
    if (!check_keyword(INPUT_VAR))
    {
        return 0;
    }
    else
    {
        pass_point(INPUT_VAR);
        extract_input(memory);
    }
    input_found = 1;
    return 1;
}

static void extract_input(memory_t *memory)
{
    if (act_char != '\'')
    {
        sprintf(err, "error: input: expected ' (L : %d, C: %d)", line, col);
        error(err);
    }
    next_char();
    int escape = 0;
    int count = 0;
    do
    {
        count++;
        go_right(act_char, memory);
        escape = next_char();
        if (act_char == '\n' && !escape)
        {
            sprintf(err, "error: input: expected ' (L : %d, C: %d)", line, col);
            error(err);
        }
    } while (act_char != '\'' || escape);
    for (size_t i = 0; i < count; i++)
    {
        go_left(-1, memory);
    }
}