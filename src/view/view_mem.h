#ifndef VIEW_MEME_H
#define VIEW_MEME_H

extern void init_meme_view();
extern void print_memory(memory_t *memory, int shift);
extern void free_view_mem();

#endif /* VIEW_MEME_H */