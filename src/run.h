#ifndef RUN_H
#define RUN_H

#include "models/memory.h"
#include "models/diagram.h"

typedef struct turing_machine_s
{
    memory_t *mem;
    diagram *diag;
    node *actual_node;
} turing_machine;

extern int runing;

extern void init_turing_machine(const char *filename);
extern void run_turing_machine();
extern void free_turing_machine();
extern int button_tap(int button);

#endif /* RUN_H */