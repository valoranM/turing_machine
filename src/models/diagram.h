#ifndef DIAGRAM_H
#define DIAGRAM_H

#define NAME_LENGTH 20

typedef struct node_s node;

enum direction
{
    LEFT,
    RIGHT
};

typedef struct link_s
{
    char IN, OUT;
    enum direction dir;
    node *node;
    struct link_s *next;
} link_t;

typedef struct node_s
{
    char name[NAME_LENGTH];
    link_t *links;
} node;

typedef struct
{
    node **nodes;
    node *start;
    int number_of_node;
} diagram;

extern diagram *init_diagram(char **node_name, int number_of_node);
extern void init_start(char *start, diagram *diag);
extern void free_diagram(diagram *diag);
extern void add_link(char *name_src, char *name_tgt, char IN, char OUT, enum direction dir, diagram *diag);

#endif /* DIAGRAM_H */