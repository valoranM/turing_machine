#include <ncurses.h>

#include "view/menu.h"
#include "run.h"

static enum menu_selected selected = QUIT;
enum stat menu_state = BREAK;

/**
 * @brief input processing
 * 
 * @return void* 
 */
void *input()
{
    while (runing)
    {

        int change = 0;
        switch (getch())
        {
        case 'q':
            runing = 0;
            break;

        case KEY_LEFT:
            selected = (selected - 1) % 4;
            change = CHANGE;
            break;

        case KEY_RIGHT:
            selected = (selected + 1) % 4;
            change = CHANGE;
            break;
        case '\n':
            change = button_tap(selected);
        default:
            break;
        };
        if (change == CHANGE)
        {
            print_menu();
        }
    }
}

static char button_name[5][11] = {"   Quit   ",
                                  "   Reset  ",
                                  "   Run    ",
                                  "   Step   ",
                                  "   Stop   "};

/**
 * @brief displays the menu below the memory strip
 * 
 */
void print_menu()
{
    start_color();
    init_pair(0, -1, COLOR_WHITE);
    init_pair(1, COLOR_WHITE, COLOR_WHITE);

    int y = LINES - 3;
    int width = 12 * 4;
    int x = COLS / 2 - width / 2;
    for (size_t i = 0; i < 4; i++)
    {
        attron(COLOR_PAIR(i == selected));
        if (i == 2 && menu_state == RUN)
        {
            mvprintw(y, x + (width / 4) * i, "%s", button_name[4]);
        }
        else
        {
            mvprintw(y, x + (width / 4) * i, "%s", button_name[i]);
        }
        attroff(COLOR_PAIR(i == selected));
    }
    refresh();
}