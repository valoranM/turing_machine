#ifndef BAND_H
#define BAND_H

typedef struct band_s
{
    char character;
    struct band_s *next, *prev;
} band_t;

typedef struct memory_s
{
    char empty;
    band_t *band;
} memory_t;

extern memory_t *setup_memory();
extern void finish_memory_setup(memory_t *memory);
extern void free_memory(memory_t *memory);
extern void go_right(char c, memory_t *memory);
extern void go_left(char c, memory_t *memory);

#endif /* BAND_H */