#ifndef BLANK_H
#define BLANK_H

#include "run.h"

extern int blank_found;

extern int is_blank(turing_machine *computer);

#endif /* BLANK_H */